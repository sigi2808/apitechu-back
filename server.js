// var express = require('express');
// var app = express();
const global = require('./global');
const userController = require('./controller/usersController');
// var bodyParser = require('body-parser');
global.app.use(global.bodyParser.json());

//Peticion GET
// app.get(URL_BASE + 'users',
global.app.get(global.URL_BASE + 'users', userController.getUsers);
//   function (req, res) {
//     res.status(202); // forzando estado solo para prueba
//     res.send(user_file);
//     //  res.send({"msg":"Operacion Get Exitosa"});
// });

// PETICION GET USERS CON ID
global.app.get(global.URL_BASE + 'users/:id', userController.getUsersId);
// function (req, res) {
//   let pos = req.params.id-1;
//   console.log("GET con id= " + req.params.id);
//   let tam = user_file.length;
//   console.log(tam);
//   //let respuesta = user_file[pos];
//   let respuesta = (user_file[pos] == undefined) ?
//     {"msg":"Usuario no encontrado"}:user_file[pos];
//   res.send(respuesta);
//
// });

//POST de users
global.app.post(global.URL_BASE + 'users', userController.addUsers);
 // function(req, res){
 //   console.log('POST de users');
 //   console.log('Nuevo usuario:' + req.body);
 //   console.log('Nuevo usuario:' + req.body.first_name);
 //   console.log('Nuevo usuario:' + req.body.email);
 //
 //   let newID = user_file.length +1;
 //   let newUser = {
 //    "id" : newID,
 //    "first_name" : req.body.first_name,
 //    "last_name" : req.body.last_name,
 //    "email" : req.body.email,
 //    "password" : req.body.password
 //   }
 //   user_file.push(newUser);
 //   console.log("nuevo usuario" + newUser);
 //   res.send(newUser);
 //   res.send({"msg":"POST exitoso"})
 // });

 //PUT de users
 global.app.put(global.URL_BASE + 'users/:id',userController.updUser);
  // function(req, res){
  //   console.log("PUT con id= " + req.params.id);
  //     console.log('upd usuario:' + req.body);
  //   console.log('upd usuario:' + req.body.first_name);
  //   console.log('upd usuario:' + req.body.email);
  //   let updUser = {
  //     "id":req.params.id,
  //    "first_name" : req.body.first_name,
  //    "last_name" : req.body.last_name,
  //    "email" : req.body.email,
  //    "password" : req.body.password
  //   }
 //
 //
 // user_file[req.params.id-1] = updUser;
 //
 //    console.log("USUARIO MODIFICADO" + updUser);
 //      res.send({"msg":"PUT exitoso"})
 //  });



// delete
global.app.delete(global.URL_BASE + 'users/:id', userController.deleteUser);
// function(req,res) {
// console.log('DELETE USERS.');
// let pos = req.params.id-1;
//
// let respuesta = (user_file[pos] == undefined) ?
//   {"msg":"Usuario no encontrado"}:user_file.splice(pos,1);
// res.send(respuesta);
//
// }
// );

//login
global.app.post(global.URL_BASE + 'login', userController.login);
// function(req,res){
//  console.log('Login');
//  console.log(req.body.email);
//  console.log(req.body.password);
// let email=req.body.email;
// let pass= req.body.password;
//  let tam = user_file.length;
//  let i= 0;
//  let encontrado = false;
//  let mensaje="";
//  while ((i < user_file.length) && !encontrado){
//    if (user_file[i].email == email)
// {
//   mensaje="";
//   if(user_file[i].password == pass){
//     encontrado=true;
//     user_file[i].logged = true;
//     mensaje="OK";
//   }else{
//     mensaje="NOK"
//   }
// }
//    i++;
//    if (encontrado)
//      res.send({"mensaje" : "Login correcto", "idUsuario" : i, "msg" : mensaje});
//    else {
//      res.send({"mensaje" : "Login incorrecto", "msg ": mensaje });
//    }
//  }
//
// });

//LOGOUT
global.app.post(global.URL_BASE + 'logout/:id', userController.logout);
// function(req,res){
//  console.log('Logout');
//  let pos = req.params.id-1;
//  let respuesta ="";
// if(user_file[pos] == undefined)
//  {
// respuesta="Usuario no encontrado";
//    }
//    else{
//      if(user_file[pos].logged==true){
//      delete user_file[pos].logged;
//      respuesta="LISTO";
//      }
//      else{
//        respuesta="NO SE ENCUENTRA LOGEADO";
//      }
//
//    }
//  res.send(respuesta);
// });

function writeUserDataToFile(data) {
   var fs = require('fs');
   var jsonUserData = JSON.stringify(data);
   fs.writeFile("./users.json", jsonUserData, "utf8",
    function(err) { //función manejadora para gestionar errores de escritura
      if(err) {
        console.log(err);
      } else {
        console.log("Datos escritos en 'users.json'.");
      }
    })
 }

global.app.listen(global.port, function () {
  console.log('Example app listening on port 3000!');
});
