//demostracion de exports
const URL_BASE='/techu-peru/v2/';
const port = process.env.PORT ||  3000;
const expressLib= require('express');
const app = expressLib();
const user_file =require('./user.json');
const bodyParser = require('body-parser');
module.exports ={
  URL_BASE, port, expressLib, app, user_file, bodyParser
}
